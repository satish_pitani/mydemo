import XCTest

import MyLibDemoTests

var tests = [XCTestCaseEntry]()
tests += MyLibDemoTests.allTests()
XCTMain(tests)
