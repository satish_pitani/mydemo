// DO NOT EDIT.
//
// Generated by the Swift generator plugin for the protocol buffer compiler.
// Source: Base/BaseResponse.proto
//
// For information on using the generated types, please see the documentation:
//   https://github.com/apple/swift-protobuf/

import Foundation
import SwiftProtobuf

// If the compiler emits an error on this type, it is because this file
// was generated by a version of the `protoc` Swift plug-in that is
// incompatible with the version of SwiftProtobuf to which you are linking.
// Please ensure that your are building against the same version of the API
// that was used to generate this file.
fileprivate struct _GeneratedWithProtocGenSwiftVersion: SwiftProtobuf.ProtobufAPIVersionCheck {
  struct _2: SwiftProtobuf.ProtobufAPIVersion_2 {}
  typealias Version = _2
}

public struct Base_ApiError {
  // SwiftProtobuf.Message conformance is added in an extension below. See the
  // `Message` and `Message+*Additions` files in the SwiftProtobuf library for
  // methods supported on all messages.

  public var errorCode: String = String()

  public var errorMessage: String = String()

  public var unknownFields = SwiftProtobuf.UnknownStorage()

  public init() {}
}

public struct Base_BaseResponse {
  // SwiftProtobuf.Message conformance is added in an extension below. See the
  // `Message` and `Message+*Additions` files in the SwiftProtobuf library for
  // methods supported on all messages.

  public var dataOrError: OneOf_DataOrError? {
    get {return _storage._dataOrError}
    set {_uniqueStorage()._dataOrError = newValue}
  }

  public var data: SwiftProtobuf.Google_Protobuf_Any {
    get {
      if case .data(let v)? = _storage._dataOrError {return v}
      return SwiftProtobuf.Google_Protobuf_Any()
    }
    set {_uniqueStorage()._dataOrError = .data(newValue)}
  }

  public var error: Base_ApiError {
    get {
      if case .error(let v)? = _storage._dataOrError {return v}
      return Base_ApiError()
    }
    set {_uniqueStorage()._dataOrError = .error(newValue)}
  }

  public var unknownFields = SwiftProtobuf.UnknownStorage()

  public enum OneOf_DataOrError: Equatable {
    case data(SwiftProtobuf.Google_Protobuf_Any)
    case error(Base_ApiError)

  #if !swift(>=4.1)
    public static func ==(lhs: Base_BaseResponse.OneOf_DataOrError, rhs: Base_BaseResponse.OneOf_DataOrError) -> Bool {
      switch (lhs, rhs) {
      case (.data(let l), .data(let r)): return l == r
      case (.error(let l), .error(let r)): return l == r
      default: return false
      }
    }
  #endif
  }

  public init() {}

  fileprivate var _storage = _StorageClass.defaultInstance
}

// MARK: - Code below here is support for the SwiftProtobuf runtime.

fileprivate let _protobuf_package = "Base"

extension Base_ApiError: SwiftProtobuf.Message, SwiftProtobuf._MessageImplementationBase, SwiftProtobuf._ProtoNameProviding {
  public static let protoMessageName: String = _protobuf_package + ".ApiError"
  public static let _protobuf_nameMap: SwiftProtobuf._NameMap = [
    1: .same(proto: "errorCode"),
    2: .same(proto: "errorMessage"),
  ]

  public mutating func decodeMessage<D: SwiftProtobuf.Decoder>(decoder: inout D) throws {
    while let fieldNumber = try decoder.nextFieldNumber() {
      switch fieldNumber {
      case 1: try decoder.decodeSingularStringField(value: &self.errorCode)
      case 2: try decoder.decodeSingularStringField(value: &self.errorMessage)
      default: break
      }
    }
  }

  public func traverse<V: SwiftProtobuf.Visitor>(visitor: inout V) throws {
    if !self.errorCode.isEmpty {
      try visitor.visitSingularStringField(value: self.errorCode, fieldNumber: 1)
    }
    if !self.errorMessage.isEmpty {
      try visitor.visitSingularStringField(value: self.errorMessage, fieldNumber: 2)
    }
    try unknownFields.traverse(visitor: &visitor)
  }

  public static func ==(lhs: Base_ApiError, rhs: Base_ApiError) -> Bool {
    if lhs.errorCode != rhs.errorCode {return false}
    if lhs.errorMessage != rhs.errorMessage {return false}
    if lhs.unknownFields != rhs.unknownFields {return false}
    return true
  }
}

extension Base_BaseResponse: SwiftProtobuf.Message, SwiftProtobuf._MessageImplementationBase, SwiftProtobuf._ProtoNameProviding {
  public static let protoMessageName: String = _protobuf_package + ".BaseResponse"
  public static let _protobuf_nameMap: SwiftProtobuf._NameMap = [
    1: .same(proto: "data"),
    2: .same(proto: "error"),
  ]

  fileprivate class _StorageClass {
    var _dataOrError: Base_BaseResponse.OneOf_DataOrError?

    static let defaultInstance = _StorageClass()

    private init() {}

    init(copying source: _StorageClass) {
      _dataOrError = source._dataOrError
    }
  }

  fileprivate mutating func _uniqueStorage() -> _StorageClass {
    if !isKnownUniquelyReferenced(&_storage) {
      _storage = _StorageClass(copying: _storage)
    }
    return _storage
  }

  public mutating func decodeMessage<D: SwiftProtobuf.Decoder>(decoder: inout D) throws {
    _ = _uniqueStorage()
    try withExtendedLifetime(_storage) { (_storage: _StorageClass) in
      while let fieldNumber = try decoder.nextFieldNumber() {
        switch fieldNumber {
        case 1:
          var v: SwiftProtobuf.Google_Protobuf_Any?
          if let current = _storage._dataOrError {
            try decoder.handleConflictingOneOf()
            if case .data(let m) = current {v = m}
          }
          try decoder.decodeSingularMessageField(value: &v)
          if let v = v {_storage._dataOrError = .data(v)}
        case 2:
          var v: Base_ApiError?
          if let current = _storage._dataOrError {
            try decoder.handleConflictingOneOf()
            if case .error(let m) = current {v = m}
          }
          try decoder.decodeSingularMessageField(value: &v)
          if let v = v {_storage._dataOrError = .error(v)}
        default: break
        }
      }
    }
  }

  public func traverse<V: SwiftProtobuf.Visitor>(visitor: inout V) throws {
    try withExtendedLifetime(_storage) { (_storage: _StorageClass) in
      switch _storage._dataOrError {
      case .data(let v)?:
        try visitor.visitSingularMessageField(value: v, fieldNumber: 1)
      case .error(let v)?:
        try visitor.visitSingularMessageField(value: v, fieldNumber: 2)
      case nil: break
      }
    }
    try unknownFields.traverse(visitor: &visitor)
  }

  public static func ==(lhs: Base_BaseResponse, rhs: Base_BaseResponse) -> Bool {
    if lhs._storage !== rhs._storage {
      let storagesAreEqual: Bool = withExtendedLifetime((lhs._storage, rhs._storage)) { (_args: (_StorageClass, _StorageClass)) in
        let _storage = _args.0
        let rhs_storage = _args.1
        if _storage._dataOrError != rhs_storage._dataOrError {return false}
        return true
      }
      if !storagesAreEqual {return false}
    }
    if lhs.unknownFields != rhs.unknownFields {return false}
    return true
  }
}
